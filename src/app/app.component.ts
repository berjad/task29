import { Component } from '@angular/core';
import { BrowserStack } from 'protractor/built/driverProviders';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public message: string = 'Hi there! I am from TS';
  public lorem: string = 'Nullam pharetra mollis leo. Nunc sed risus a diam dictum tempor vel ut erat. Curabitur ut diam elit. Cras mauris magna, ultrices sit amet imperdiet sit amet, facilisis nec nulla. Duis efficitur ac dui ac porttitor.'
  public bond: number = 7;
  public pi: number = 3.141592653589793;
  public slayerAlbums: string [] = [
    'Reign in Blood',
    'South of Heaven',
    'Repentless'
  ];
  // public aboutMe: any;
  //   constructor() {
  //     this.aboutMe = {
  //       Name: 'Berra',
  //       Surname: 'Jäderlund',
  //       Age: 48
  // };
  public aboutMe: any = {
    Name: 'Berra',
    Surname: 'Jäderlund',
    Age: 48
};

unsorted(a, b) {
  return 1;
}

}
