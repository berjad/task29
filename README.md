# Task29 - Angular Basics

- Install the Angular CLI (globally)
- Create a new Angular project using the CLI
- Ensure the project can run 
- Remove the unnecessary html from the app.component.html
- Create a string, number, array of strings and Object in the app.component.ts file
- There is no predetermined variables or data, make up your own.
- Display the values from each of these objects in the app.component.html file
- For the array , loop over the array and display all the values in the array
- For the object, if you create and object with three properties i.e. user = { name: 'Dewald', surname: 'Els', age: - 'Unknown' } - You must display the values of all three properties with their corresponding name. So the HTML should display name: Dewald, surname: Els etc.